function getClientes()
{
 var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
 var request = new XMLHttpRequest();
 request.onreadystatechange = function()
{
   if (this.readyState == 4 && this.status == 200)
   {
     console.log(request.responseText);
     clientesObtenidos = request.responseText;
     procesarClientes();
   }
 }
 request.open("GET",url,true);
 request.send();
}
function procesarClientes()
{
  var JSONClientes = JSON.parse(clientesObtenidos);
  var tabla = document.getElementById("tablaClientes");

for (var i = 0; i < JSONClientes.value.length; i++)
{
  var nuevaFila = document.createElement("tr");

  var columnaNombre = document.createElement("td");
  columnaNombre.innerText = JSONClientes.value[i].ContactName;

  var columnaDireccion = document.createElement("td");
  columnaDireccion.innerText = JSONClientes.value[i].Address;

  var columnaCompanyName = document.createElement("td");
  var imgBandera = document.createElement("img");


  imgBandera.classList.add("flag");
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var extension = ".png";
  
  if (JSONClientes.value[i].Country=="UK")
  {      imgBandera.src = rutaBandera + "United-Kingdom" + extension;
}
  else {

        imgBandera.src = rutaBandera + JSONClientes.value[i].Country + extension;
  }



  var columnaBandera = document.createElement("td");

  columnaBandera.appendChild(imgBandera);

  nuevaFila.appendChild(columnaNombre);
  nuevaFila.appendChild(columnaDireccion);
  nuevaFila.appendChild(columnaCompanyName);
  nuevaFila.appendChild(columnaBandera);
  tabla.appendChild(nuevaFila);

  console.log(JSONClientes.value[i].ContactName);
}
}
